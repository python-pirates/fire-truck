graph=[[] for i in range(60)]
visited=[False]*60
ans=[]
def dfs(start, dest, path):
    if (start == dest):
        ans.append(path)
    for i in graph[start]:
        if (visited[i] == False):
            dfs(i, dest, path + [i])
            visited[i] = False


testcases  = int(input())
for i in range(testcases):
    dest = int(input())
