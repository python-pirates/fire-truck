graph = [[] for i in range(60)]
visited = [False]*60
ans = []

def dfs(start, dest, path):
    if (start == dest):
        ans.append(path)
    for i in graph[start]:
        if (visited[i] == False):
            visited[i] = True
            dfs(i, dest, path + [i])
            visited[i] = False


testcases = int(input())
for i in range(testcases):
    dest = int(input())
    while (True):
        x, y = map(int, input().split())
    visited[1] = True
    dfs(1, dest, [1])
    visited[1] = False
    print("CASE", str(i + 1) + ":")
    for j in ans:
        for k in j:
            print(k, end=" ")
        print()
    print("There are", len(ans), "routes from the firestation to streetcorner", str(dest) + ".")